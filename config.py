import os
from decouple import config


class Config(object):
    """
    Set of configurations that will be
    used across the application
    """

    PHABRICATOR_KEY = config("PHABRICATOR_KEY")
    PHABRICATOR_URI = config("PHABRICATOR_URI")
