# WMF AppSec Phabricator Bot

## Why
This bot is intended to take over some of the manual steps included in the general [Phabricator Workboard Maintenance](https://office.wikimedia.org/wiki/Wikimedia_Security_Team/Phabricator_Workboard_Maintenance) workflow.

## Requirements
* [Python 3.x+](https://www.python.org/downloads/)
* [PIP (Python Dependency Manager)](https://pip.pypa.io/en/stable/installing/)

## Install dependencies
Install relevant dependencies with `pip install -r requirements.txt`.

## Set environment variables
Rename the `.env.example` file into `.env` and modify it according to your Phabricator API key and related configurations.

## Running tests
Tests are found under the `tests` folder.
You can run them with [pytest](https://docs.pytest.org/en/6.2.x/getting-started.html) using `pytest tests/`
