from config import Config
from task import Task
from datetime import datetime
from phabricator import Phabricator

class PhabricatorApi:
    def __init__(self):
        self.client = Phabricator(
            host=Config.PHABRICATOR_URI,
            token=Config.PHABRICATOR_KEY,
            timeout=30,
        )

    def get_tasks(self, query_key):
        """
        Arbitrarily narrow down to only one workspace
        :param limit: how many tasks to collect
        """
        task_list = []
        temp_task_list = []
        cursor_overflow = True
        cursor_after = None

        # Bypass 100-result limit set by the API
        while True:

            # Check whether resultset is  larger than limit
            if cursor_after is None:
                result = self.client.maniphest.search(
                    queryKey=query_key,
                    attachments={"projects": True},
                )

            else:
                result = self.client.maniphest.search(
                    queryKey=query_key,
                    attachments={"projects": True},
                    after=cursor_after,
                )

            temp_task_list = temp_task_list + result["data"]

            # Update cursor details
            cursor_after = result["cursor"]["after"]
            if cursor_after is None:
                break

        # Parse JSON into Task objects
        for task in temp_task_list:
            task_list.append(
                Task(
                    task["id"],
                    task["fields"]["name"].strip(),
                    "{}".format(Config.PHABRICATOR_URI.replace("/api/", ""))
                    + "/T"
                    + str(task["id"]),
                    (
                        type(task["fields"]["dateClosed"]) is int
                    ),
                    datetime.strftime(
                        datetime.fromtimestamp(task["fields"]["dateCreated"]),
                        "%Y-%m-%d %H:%m:%S",
                    ),
                    task["attachments"]["projects"]["projectPHIDs"],
                )
            )

        return task_list
