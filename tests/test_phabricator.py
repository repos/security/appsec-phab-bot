from phabricator_api import PhabricatorApi

class TestPhabricator:
    """
    Test suite for operations related to the
    Phabricator API
    """

    def test_get_tasks(self):
        query_key="d8PfmjQXvfN5"
        tasks = PhabricatorApi().get_tasks(query_key)
        print(tasks)
        
        assert type(tasks) is list
