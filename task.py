class Task:
    """
    Abstract data model providing a unified way to represent
    tasks fetched from various providers
    """

    def __init__(self, id, title, link, is_completed, created_at, project_tags):
        """
        @param: id Task identifier
        @param: title Title of the task
        @param: link URI for accessing the task
        @param: is_completed Closure status
        @param: created_at When the task was first created
        @param: project_tags Project tags for the task
        """
        self.id = id
        self.title = title
        self.link = link
        self.is_completed = is_completed
        self.created_at = created_at
        self.project_tags = project_tags

    def __repr__(self):
        return "<Task {} | {} | {} | {} | {} | {} >".format(
            self.id,
            self.title,
            self.link,
            self.is_completed,
            self.created_at,
            self.project_tags,
        )
